import java.util.Scanner;

public class Prime{
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args){
		System.out.print("Please enter number: ");
		int value = input.nextInt();

		if(isPrime(value)){
			System.out.println(value + " is a prime number");
		}
		else{
			System.out.println(value + " is not a prime number");
		}

	}

	public static boolean isPrime(int number){
		if(number <= 1){
			return false;
		}
		if(number ==  2){
			return true;
		}
		
		for(int i = 2; i*i <= number; i++){
			if(number%i == 0){
				return false;
			}
		}
		return true;
	}
}