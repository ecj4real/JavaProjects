public class InverseStars{
	public static void main(String[] args){
		for(int i = 20, j=0; i > 0; i--,j++){
			for(int k = 0; k < j; k++){
				System.out.print("  ");
			}
			for(int l = i; l > 0; l--){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}