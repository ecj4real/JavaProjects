import java.util.Arrays;

public class BinarySearch{
	public static void main(String[] args){
		int[] numbers = {2,3,4,1,8,10,5};
		Arrays.sort(numbers);
		System.out.println(Arrays.toString(numbers));

		int value = 6;

		int index = search(numbers, value, 0, numbers.length - 1);

		if(index == -1){
			System.out.println("The number " + value + " is not in the List");
		}
		else{
			System.out.println("The number " + value + " is at index position: " + index);
		}
	}
	public static int search(int[] arr, int value, int low, int high){
		if(high < low){
			return -1;
		}

		int mid = (low + high) / 2;

		if(arr[mid] > value){
			return search(arr, value, low, mid - 1);
		}
		else if(arr[mid] < value){
			return search(arr, value, mid + 1, high);
		}
		else{
			return mid;
		}
	}
}