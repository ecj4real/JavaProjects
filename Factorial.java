import java.util.Scanner;
import java.math.BigInteger;

public class Factorial{
	private static Scanner input = new Scanner(System.in);

	public  static void main(String[] args){
		System.out.print("Enter the number: ");
		int number = input.nextInt();
		if(number < 0){
			System.out.println("Number must be greater than or equal to ZERO");
		}
		else{
			BigInteger input = new BigInteger(""+number);
			System.out.println("The factorial of " + number + " is: " + fac(input));
		}
	}

	public static BigInteger fac(BigInteger n){
		if(n.equals(BigInteger.ZERO)){
			return BigInteger.ONE;
		}
		else if(n.equals(BigInteger.ONE)){
			return BigInteger.ONE;
		}
		else{
			return n.multiply(fac(n.subtract(BigInteger.ONE)));
		}
	}
}