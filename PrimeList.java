import java.util.Scanner;
import java.util.ArrayList;

public class PrimeList{
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args){
		System.out.print("Please enter the First Number: ");
		int first = input.nextInt();
		System.out.print("Please enter the Last Number: ");
		int last = input.nextInt();
		System.out.println();

		if(first > last){
			System.out.println("Sorry, Last Number should be greater than First Number");
		}
		else{
			ArrayList<Integer> primes = new ArrayList<Integer>();

			for(int i = first; i <= last; i++){
				if(isPrime(i)){
					primes.add(i);
				}
			}
			if(primes.size() == 0){
				System.out.println("No prime number exists betweeen the numbers");
			}
			else{
				System.out.println("The prime pumber(s) is/are: " + primes);
			}
		}

	}

	public static boolean isPrime(int number){
		if(number <= 1){
			return false;
		}
		if(number ==  2){
			return true;
		}
		
		for(int i = 2; i*i <= number; i++){
			if(number%i == 0){
				return false;
			}
		}
		return true;
	}
}