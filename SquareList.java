import java.util.Scanner;
import java.util.Arrays;

public class SquareList{
	private static Scanner input =  new Scanner(System.in);

	public static void main(String[] args){
		System.out.print("Enter number of values: ");
		int listLength = input.nextInt();

		int[] numbers = new int[listLength];
		int[] squares = new int[listLength];
		int[] cubes = new int[listLength];

		for(int i = 0; i < listLength; i++){
			System.out.print("Enter number " + (i+1) + ": ");
			numbers[i] = input.nextInt();

			squares[i] = numbers[i] * numbers[i];
			cubes[i] = numbers[i] * numbers[i] * numbers[i];
		}

		System.out.println("Numbers entered is: " + Arrays.toString(numbers));
		System.out.println("Square of numbers: " + Arrays.toString(squares));
		System.out.println("cubes of numbers: " + Arrays.toString(cubes));

	}

}