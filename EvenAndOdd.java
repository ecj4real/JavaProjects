import java.util.Scanner;
import java.util.ArrayList;

public class EvenAndOdd{
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args){
		System.out.print("Please enter the first number: ");
		int firstNumber = input.nextInt();

		System.out.print("Please enter the last number: ");
		int lastNumber = input.nextInt();

		if(lastNumber < firstNumber)
		{
			System.out.println("First Number shouldn't be greater than last number");
		}
		else if( firstNumber < 0 || lastNumber < 0){
			System.out.println("Please enter a valid positive integer");
		}
		else{
			ArrayList<Integer> odd = new ArrayList<Integer>();
			ArrayList<Integer> even = new ArrayList<Integer>();

			do{
				if(firstNumber%2 == 0){
					even.add(firstNumber);
				}
				else{
					odd.add(firstNumber);
				}
				firstNumber++;	
			}while(firstNumber <= lastNumber);

			System.out.println("Even numbers: " + even);
			System.out.println("Odd numbers: " + odd);
		}
	}
	
}