import java.util.Arrays;
import java.util.ArrayList;

public class MergeSort{
	public static void main(String[] args){
		int[] unsort = {6,3,4,2,3,55,4,3,2,66,7,8,20};

		System.out.println("Unsorted List:\t" + Arrays.toString(unsort));
		System.out.println("\nSorted List:\t" + Arrays.toString(merge(unsort)));
		
	}

	public static int[] merge(int[] unsorted){
		int lengthOfUnsorted = unsorted.length;

		if(lengthOfUnsorted == 1){
			return unsorted;
		}

		int[] firstHalf = Arrays.copyOfRange(unsorted, 0, lengthOfUnsorted/2);
		int[] secondHalf = Arrays.copyOfRange(unsorted, lengthOfUnsorted/2, lengthOfUnsorted);
		
		firstHalf = merge(firstHalf);
		secondHalf = merge(secondHalf);
		
		return sort(firstHalf, secondHalf);
	}

	public static int[] sort(int[] a, int[] b){
		ArrayList<Integer> partA = new ArrayList<Integer>();
		ArrayList<Integer>  partB = new ArrayList<Integer>();
		
		ArrayList<Integer> output = new ArrayList<Integer>();

		for(int i=0; i < a.length; i++){
			partA.add(a[i]);
		}
		for(int i=0; i < b.length; i++){
			partB.add(b[i]);
		}

		while(!partA.isEmpty() && !partB.isEmpty()){
			if(partA.get(0) > partB.get(0)){
				output.add(partB.get(0));
				partB.remove(0);
			}
			else{
				output.add(partA.get(0));
				partA.remove(0);
			}
		}
		
		while(!partA.isEmpty()){
			output.add(partA.get(0));
			partA.remove(0);
		}
		
		while(!partB.isEmpty()){
			output.add(partB.get(0));
			partB.remove(0);
		}
		
		int[] sorted = new int[output.size()];
		for(int i = 0; i < sorted.length; i++){
			sorted[i] = output.get(i).intValue();
		}
		
		return sorted;
	}
}
